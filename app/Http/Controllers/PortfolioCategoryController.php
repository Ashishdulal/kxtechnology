<?php

namespace App\Http\Controllers;

use App\portfolioCategory;
use Illuminate\Http\Request;

class PortfolioCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolioCategories = portfolioCategory::all();
        return view ('dashboard.portfolio.portfolio-category.index', compact('portfolioCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.portfolio.portfolio-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $portfolioCategories = new portfolioCategory();

        $portfolioCategories->title = request('title');

        $portfolioCategories->save();

        return redirect('/home/portfolio-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\portfolioCategory  $portfolioCategory
     * @return \Illuminate\Http\Response
     */
    public function show(portfolioCategory $portfolioCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\portfolioCategory  $portfolioCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(portfolioCategory $portfolioCategory,$id)
    {
        $portfolioCategories = portfolioCategory::findOrfail($id);
        return view ('/dashboard/portfolio/portfolio-category/edit',compact('portfolioCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\portfolioCategory  $portfolioCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, portfolioCategory $portfolioCategory,$id)
    {
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $portfolioCategories = portfolioCategory::findOrFail($id);

        $portfolioCategories->title = request('title');

        $portfolioCategories->save();

        return redirect('/home/portfolio-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\portfolioCategory  $portfolioCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolioCategories = portfolioCategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
