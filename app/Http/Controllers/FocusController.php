<?php

namespace App\Http\Controllers;

use App\Focus;
use Illuminate\Http\Request;

class FocusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $focus = Focus::all();
        return view ('dashboard.focus.index',compact('focus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view ('dashboard.focus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $focus = new Focus();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $focus->name = $request->name;
        $focus->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "focus".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $focus->image = $image;
        }
        else{
            $focus->image = 'default-thumbnail.png';
        }        
        $focus->save();
        return redirect('/home/our-focus');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Focus  $focus
     * @return \Illuminate\Http\Response
     */
    public function show(Focus $focus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Focus  $focus
     * @return \Illuminate\Http\Response
     */
    public function edit(Focus $focus,$id)
    {
        $focus = Focus::findOrFail($id);
        return view ('dashboard.focus.edit',compact('focus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Focus  $focus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Focus $focus,$id)
    {
        $focus = Focus::findOrFail($id);
         $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $focus->name = $request->name;
        $focus->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "focus".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $focus->image = $image;
        }
        else{
            $focus->image = $focus->image;
        }        
        $focus->save();
        return redirect('/home/our-focus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Focus  $focus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $focus = Focus::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
