<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  
use App\Mail\SendMail;

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'service' =>  'required',
      'phone' =>  'required',
      'subject' =>  'required',
     ]);

        $data = array(
            'name'      =>  $request->name,
            'service'   =>   $request->service,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'subject'   =>   $request->subject,
            'messages'   =>   $request->messages,
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>'. $data['email'] .'<br><br>'. $data['phone'] .'</p>
                    <p>I have query for the '. $data['service'] .' service in the subject '.$data['subject'].'<br> <br>
                    '. $data['messages'] .'.</p>
                    <p>It would be appriciative, if i receive the appointment soon.</p>
        </body>
        </html>';       

        $to = "azizdulal.ad@gmail.com";
        $subject = "Appointment Request";

        $headers = "From:Kxtechnologies.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }


function subscribe(Request $request)
    {
     $this->validate($request, [
      'email'  =>  'required|email',
     ]);

        $sdata = array(
            'email'   =>   $request->email,
        );
        
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $sdata['email'] .'</p>
                    <p>I want to subscribe for the newsletter.</p>
                    <p>It would be appriciative, if i receive the subscription soon.</p>
        </body>
        </html>';       

        $to = "azizdulal.ad@gmail.com";
        $subject = "Appointment Request";

        $headers = "From:kxtechnology.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt2,$headers);
                 return back()->with('success','Thanks for subscribing us!');
        }

  




}
